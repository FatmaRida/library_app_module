# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class CreateCheckout(models.TransientModel):
    _name = 'create.checkout'

    name = fields.Char(string='Book Name',track_visibility = 'always')
    Publisher_id = fields.Many2one('library.publishers', string='Publisher')
    date = fields.Datetime('CheckOut Date')


    # create from wizard into another model
    # @api.model_create_multi
    # def create(self, vals_list=[]):
    #     print(" before edit values", vals_list)
    #     print("self", self)
    #     # this is the way to declaring before super method
    #     print(" after edit values", vals_list)
    #     return super(CreateCheckout, self).create(vals_list)
    #     # this is the way to declaring after super method




