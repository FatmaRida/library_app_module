from odoo import http
from odoo.http import request


# define two controller first controller for rendering(displaying) page

class Request(http.Controller):

    @http.route('/user/webform', auth='public',website=True,type="http")
    def user_webform(self, **kw):
        print("Exception here..........")
        return http.request.render('new_library.create_user',{})

    # second controller controller for creating record in odoo backend
    @http.route('/create/webUser', auth='public', website=True , type="http")
    def create_webuser(self, **kw):
        print("data received..........",kw)
        request.env['library.books'].sudo().create(kw)
        return request.render('new_library.user_thanks_page', {})



