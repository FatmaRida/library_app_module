from odoo import http
from odoo.http import request

class NewLibrary(http.Controller):

  @http.route('/library/books/', website=True,auth="public")
  def library_books(self, **kw):

        book1= request.env['res.partner'].sudo().search([])

        print("books.......",book1)
        return request.render("new_library.newlibrary_books",{
            'books':book1
        })
