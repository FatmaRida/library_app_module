# -*- coding: utf-8 -*-

from odoo import models, fields


class Publishers(models.Model):
    _name = 'library.publishers'

    name = fields.Char(string='Name')
    book_ids = fields.One2many('library.books','Publisher_id',string='Books')
    # address fields
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict',
                               domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')
