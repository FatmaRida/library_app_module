# -*- coding: utf-8 -*-

from odoo import models, fields, api,_


# I want to override the create function

class ResPartner(models.Model):
      _inherit = 'res.partner'

      @api.model_create_multi
      def create(self,vals_list):
          print(" before edit values",vals_list)
          print("self",self)
          # this is the way to declaring before super method
          # vals_list['comment'] = 'hi'
          print(" after edit values",vals_list)
          res = super(ResPartner,self).create(vals_list)
          # this is the way to declaring after super method
          res.comment='people'
          print("yes working",res)
          return res

class maintenanceRequestInherit(models.Model):
    _inherit = 'maintenance.request'


    gender = fields.Selection([
        ('m','male'),
        ('f','female'),

    ], default='m')
    military = fields.Binary('Military certificate')


class Book(models.Model):
    _name = 'library.books'
    _inherit = ['mail.thread', 'mail.activity.mixin']


    name = fields.Char(string='Book Name',track_visibility = 'always')
    usr_name=fields.Char()
    email = fields.Char(string='Email')
    age = fields.Integer(track_visibility = 'always',sum="")
    price = fields.Integer(track_visibility = 'always',sum="Total of Prices")
    Author_id = fields.Many2many('res.partner', string='Author')
    Publisher_id = fields.Many2one('library.publishers', string='Publisher')
    Image = fields.Binary('Cover')
    military = fields.Binary('Military certificate')
    Category_id = fields.Many2one('library.categories', string='Category')
    gender = fields.Selection([
        ('m', 'male'),
        ('f', 'female')
    ])

    age_group = fields.Selection([
        ('major', 'Major'),
        ('minor', 'Minor'),
    ],compute='set_age_group')

    # set computed counter in button
    checkout_count = fields.Integer(compute='get_checkout_count')

    # track_visibility :to show what happend in fields
    pages_numbers = fields.Char(string='Pages Number')
    published_date = fields.Date()
    language = fields.Char()
    note = fields.Text()
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('done', 'Done'),
        ('cancel', 'Cancel')],
        string='Status', readonly=True, default='draft')
    name_seq = fields.Char(string='Sequence', required=True, copy=False, readonly=True, index=True,
     default = lambda self: _('New'))

    # create manual sequence that delete the sequence of deleted records
    # book_str :the character before number 'SE01'
    book_str = fields.Char()
    # book_sequence is the field that makes the whole operation(max_num)
    book_sequence = fields.Char()
    # book_code  :only for concatination
    book_code = fields.Char()

    @api.model
    def default_get(self, fields):
        res =super(Book, self).default_get(fields)

        books = self.env['library.books'].search([])
        list=[]
        book_str = 'B0'
        for line in books:
            list.append(int(line.book_sequence))

        if list:
            max_num = max(list)
        else:
            max_num = 0

        print(max_num)
        res['book_sequence'] = str(int(max_num)+1)
        res['book_code'] = book_str + str(int(max_num)+1)
        # res['book_code'] = book_str + res['book_sequence']


        return res






        

    # set sequence
    # @api.model
    # def create(self, vals):
    #     if vals.get('name_seq', _('New')) == _('New'):
    #         vals['name_seq'] = self.env['ir.sequence'].next_by_code('library.book.sequence')
    #
    #     result = super(Book, self).create(vals)
    #     return result


    # set computed field for age_group
    @api.depends('age')
    def set_age_group(self):
        self.age_group = ''
        for record in self:
            if record.age:
                if record.age < 18:
                    record.age_group = 'minor'
                else:
                    record.age_group = 'major'

        # set (method return action) function name for smart button
    def open_book_checkouts(self):
        return {
            'name': _('checkouts'),
            'domain': [('book_id', '=', self.id)],
            'res_model': 'library.checkout',
            'view_id': False,
            'view_mode': 'kanban,tree,form',
            'type': 'ir.actions.act_window',
        }
    # set computed counter in button


    def get_checkout_count(self):
        count = self.env['library.checkout'].search_count([('book_id', '=', self.id)])
        self.checkout_count = count
        # set actions for button
    def action_confirm(self):
        for rec in self:
            rec.state='confirm'


    def action_done(self):
        for rec in self:
            rec.state = 'done'

    def action_cancel(self):
        for rec in self:
            rec.state='cancel'
    def set_draft(self):
        for rec in self:
            rec.state='draft'

