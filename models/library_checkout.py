# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class Librarycheckout(models.Model):
    _name = 'library.checkout'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    checkout_date = fields.Date()

    _order = "checkout_date desc"
    # in many2one field I must return the id of the record
    # def get_dafault_number(self):
    #    return 1

    pages_numbers = fields.Char(string='checkout Id',track_visibility ='always')

    # set default value for many2one field :here it will return the id of record
    # , default = get_dafault_number
    book_id = fields.Many2one('library.books',string='Books',required=True)
    age = fields.Integer('Age',related='book_id.age')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('done', 'Done'),
        ('cancel', 'Cancel')],
        string='Status', readonly=True, default='draft')
    description = fields.Text('Description')




    # Control States and Statusbar Using Buttons
    def action_confirm(self):
        for rec in self:
            rec.state='confirm'


    def action_done(self):
        for rec in self:
            rec.state = 'done'


