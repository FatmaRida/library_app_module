# -*- coding: utf-8 -*-

from odoo import models, fields, api , _
from odoo.exceptions import ValidationError

class Category(models.Model):
    _name = 'library.categories'

    name = fields.Char(string='Category')
    # age = fields.Integer()
    book_limit = fields.Integer()
    book_ids = fields.One2many('library.books','Category_id',string='Books')

    # set constrains to fields
    @api.constrains('book_limit')
    def check_book_limit(self):
        for rec in self:
            if rec.book_limit >=5:
                raise ValidationError(_("book_limit must not exceed 4"))
