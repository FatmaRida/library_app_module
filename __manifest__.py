# -*- coding: utf-8 -*-
{
    'name': "newLibrary",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website','maintenance'],

    # always loaded
    'data': [
        'security/res_group.xml',
        'security/ir.model.access.csv',
        'data/sequence.xml',
        'data/data.xml',
        'wizard/create_checkout.xml',
        'views/book_view.xml',
        'views/categories_view.xml',
        'views/publisher_view.xml',
        'views/controller_view.xml',
        'views/checkout_view.xml',
        'views/website_form.xml',
        'views/change_field_view.xml',
        'reports/report.xml',
        'reports/report_template.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
